
String.prototype.format = function () {
    var str = this;
    for (var i = 0; i < arguments.length; i++) {
        str = str.replace('{' + i + '}', arguments[i]);
    }
    return str;
};


var transformControl;

var ARC_SEGMENTS = 200;
var splines = {};

var geometry = new THREE.BoxBufferGeometry(20, 20, 20);
var splinePointsLength = 20;
var point = new THREE.Vector3();
var positions = [];
var splineHelperObjects = [], spotSplineHelperObjects = [],  splineOutline;


var params = {
    uniform: true,
    tension: 0.5,
    centripetal: true,
    chordal: true,
    addPoint: addPoint,
    removePoint: 'removePoint',
    exportSpline: 'exportSpline',
    quit: QuitApp,
};

init();
animate();

function render() {
    renderer.render(scene, camera);
}

function animate() {
    requestAnimationFrame(animate);
    render();
    stats.update();
}

function init() {


    craftwindowapp.init_renderer();

    craftwindowapp.create_scene();

    craftwindowapp.create_camera();

    craftwindowapp.create_spotLight();

    craftwindowapp.create_plane();

    craftwindowapp.create_gridHelper();

    craftwindowapp.create_axesHelper();

    create_gui();

    craftwindowapp.control_scene();


     addSplineObject(positions);
    /*
    var t = 0;
    var pFinal = {};

    var p0 = new THREE.Vector3( 
        Math.random() * 1000 - 500, 
        Math.random() * 700 - 500, 
        Math.random() * 700 - 400
    );

    var p1 = new THREE.Vector3( 
        Math.random() * 1000 - 500, 
        Math.random() * 700 - 500, 
        Math.random() * 700 - 400
    );

    var p2 = new THREE.Vector3( 
        Math.random() * 1000 - 500, 
        Math.random() * 700 - 500, 
        Math.random() * 700 - 400
    );

    var p3 = new THREE.Vector3( 
        Math.random() * 1000 - 500, 
        Math.random() * 700 - 500, 
        Math.random() * 700 - 400
    );

    for( t = 0; t < 1; t += 0.05 ){
        addSplineObject_back( utils.cubicBezier(p0, p1, p2, p3, t, pFinal) );
    } */





    positions = [];

    for (var i = 0; i < splineHelperObjects.length; i++) {
        positions.push(splineHelperObjects[i].position);
    }
 
    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute('position',
        new THREE.BufferAttribute(
            new Float32Array(ARC_SEGMENTS * 3),
            3
        )
    );
   
    var curve = new THREE.CatmullRomCurve3( positions );
    curve.curveType = 'catmullrom';
    curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
       color: 0xff0000,
       opacity: 0.35,
       linewidth: 2
    } ) );
    curve.mesh.castShadow = true;
    splines.uniform = curve;
   
  
    
    for ( var k in splines ) {
        var spline = splines[ k ];
        scene.add( spline.mesh );
    } 
    
   /* addSplineObject_ControlPoint( p0 );
    addSplineObject_ControlPoint( p1 );
    addSplineObject_ControlPoint( p2 );  
    addSplineObject_ControlPoint( p3 ); 

    addSplineObject_LinesControle( p0, p1, p2, p3 )
*/

   
}

function create_gui() {
    // A lightweight graphical user interface for changing variables in JavaScript.
    // https://github.com/dataarts/dat.gui/blob/master/API.md
    var gui = new dat.GUI();
    gui.add(params, 'uniform');
    gui.add(params, 'tension', 0, 1).step(0.01).onChange(function (value) {
        splines.uniform.tension = value;
        updateSplineOutline();
    });
    gui.add(params, 'centripetal');
    gui.add(params, 'chordal');
    gui.add(params, 'addPoint');
    gui.add(params, 'removePoint');
    gui.add(params, 'exportSpline');
    gui.add(params, 'quit');
    gui.open();
}


function QuitApp() {

    window.close();
}



function addSplineObject(position) {

    var material = new THREE.MeshLambertMaterial(
        { color: Math.random() * 0xffffff }
    );   

    for (var i = 0; i < splinePointsLength; i++) {

        var object = new THREE.Mesh(geometry, material);

        if (position[i]) {

            object.position.copy(position[i]);
    
        } else {
    
            object.position.x = Math.random() * 1000 - 500;
            object.position.y = Math.random() * 700 - 500;
            object.position.z = Math.random() * 700 - 400;
    
        }

        object.castShadow = true;
        object.receiveShadow = true;

        spotSplineHelperObjects.push( object );
       
   }

   
   var spots = utils.stackPosition( spotSplineHelperObjects );

   splinePointsLength = spots.length;

   for (var i = 0; i < splinePointsLength; i++) {

        var material = new THREE.MeshLambertMaterial(
            { color: Math.random() * 0xffffff }
        );
        var object_new = new THREE.Mesh(geometry, material);
        
        if (spots[i]) {

            object_new.position.copy(spots[i]);
    
        } 

        object_new.castShadow = true;
        object_new.receiveShadow = true;
        scene.add(object_new);

        splineHelperObjects.push( object_new );
       
    }

    //scene.add(object);
    //splineHelperObjects.push(object);
    //return object;

}



function addPoint() {

    splinePointsLength++;

    positions.push(addSplineObject().position);

    updateSplineOutline();

}

function updateSplineOutline() {
    for (var k in splines) {
        var spline = splines[k];
        var splineMesh = spline.mesh;
        var position = splineMesh.geometry.attributes.position;
        for (var i = 0; i < ARC_SEGMENTS; i++) {
            var t = i / (ARC_SEGMENTS - 1);
            spline.getPoint(t, point);
            position.setXYZ(i, point.x, point.y, point.z);
        }
        position.needsUpdate = true;
    }
}


function addSplineObject_back(position) {

    var material = new THREE.MeshLambertMaterial(
        { color: Math.random() * 0xffffff }
    );
    var object = new THREE.Mesh(geometry, material);


    if (position) {

        object.position.copy(position);

    } else {

        object.position.x = Math.random() * 1000 - 500;
        object.position.y = Math.random() * 700 - 500;
        object.position.z = Math.random() * 700 - 400;

    }

    object.castShadow = true;
    object.receiveShadow = true;
    scene.add(object);
    splineHelperObjects.push(object);
    //return object;

}

function addSplineObject_ControlPoint(position) {

    var material = new THREE.MeshLambertMaterial(
        { color: 0x000000 }
    );
    var object = new THREE.Mesh(geometry, material);


    if (position) {

        object.position.copy(position);

    }

    object.castShadow = true;
    object.receiveShadow = true;
    scene.add(object);

}

function addSplineObject_LinesControle( p0, p1, p2, p3 ) {

    var material = new THREE.MeshLambertMaterial(
        { color: 0xf90000 }
    );

    var geometry = new THREE.Geometry();
    geometry.vertices.push( p0 );
    geometry.vertices.push( p1 );
    geometry.vertices.push( p2 );
    geometry.vertices.push( p3 );

    var line = new THREE.Line( geometry, material );

   
    scene.add(line);

}