
var bezier_curves = {};

var ARC_SEGMENTS = 200;

var utils = {

    stackPosition: function( spotSpline  ){

        var positions = [];
        
        if( spotSpline.length >= 5 ){

            var position_0 = spotSpline[0].position;

            positions.push( position_0 );

            for( var i = 1; i < spotSpline.length - 2; i+=1 ){

                var mid_point_pos = new THREE.Vector3();
                var controlp_init = new THREE.Vector3();
                var controlp_next = new THREE.Vector3() ;

               controlp_init = spotSpline[i].position;
               controlp_next = spotSpline[i+1].position;

               mid_point_pos.x = ( controlp_init.x + controlp_next.x ) / 2;
               mid_point_pos.y = ( controlp_init.y + controlp_next.y ) / 2;
               mid_point_pos.z = ( controlp_init.z + controlp_next.z ) / 2;

               positions.push( mid_point_pos );

            }

            positions.push( spotSpline[ spotSpline.length - 1 ].position );

        }

    /*    var objectPoint;
     
        for( spot in positions){
            objectPoint =  this.drawSpot( positions[spot], spotSpline );
        }
     */   
        
        return positions;
    },

    
    drawSpot: function ( position, spotContainer ) {

        var material = new THREE.MeshLambertMaterial(
            { color: Math.random() * 0xffffff }
        );
        var object = new THREE.Mesh(geometry, material);
    
        if ( position ) {
    
            object.position.copy(position);
            
        } 

        object.castShadow = true;
        object.receiveShadow = true;
        //scene.add(object);
        spotContainer.push(object);
        return object;
    },
    
    quadraticBezier: function( p0, p1, p2, t, pFinal ){
        
        pFinal = pFinal || {};
        
        pFinal.x = Math.pow(1 - t, 2) * p0.x + 
                           (1 - t) * 2 * t * p1.x + 
                           t * t * p2.x;
        
        pFinal.y = Math.pow(1 - t, 2) * p0.y + 
                           (1 - t) * 2 * t * p1.y + 
                           t * t * p2.y;

        pFinal.z = Math.pow(1 - t, 2) * p0.z + 
                           (1 - t) * 2 * t * p1.z + 
                           t * t * p2.z;
        
        return pFinal;
    },

   cubicBezier: function( p0, p1, p2, p3, t, pFinal  ){

        pFinal.x = Math.pow(1 - t, 3) * p0.x + 
                   Math.pow(1 - t, 2) * 3 * t * p1.x + 
                   ( 1 - t ) * 3 * t * t * p2.x + 
                    t * t * t * p3.x;

        pFinal.y = Math.pow(1 - t, 3) * p0.y + 
                    Math.pow(1 - t, 2) * 3 * t * p1.y + 
                    ( 1 - t ) * 3 * t * t * p2.y + 
                     t * t * t * p3.y;

        pFinal.z = Math.pow(1 - t, 3) * p0.z + 
                     Math.pow(1 - t, 2) * 3 * t * p1.z + 
                     ( 1 - t ) * 3 * t * t * p2.z + 
                     t * t * t * p3.z;

        return pFinal;
    }, 

    multicurve: function( points, scene ){

        var p0, p1, midx, midy, midz; 

        var cp, mid; 

        var curvePoints = [];

       var startPoint = new THREE.Vector3(
            points[0].x,
            points[0].y,
            points[0].z
        );

        curvePoints.push( startPoint );

		for(var i = 1; i < points.length - 2; i += 1) {

			p0 = points[i];
            p1 = points[i + 1];


            mid = new THREE.Vector3(
                (p0.x + p1.x) / 2,
                (p0.y + p1.y) / 2,
                (p0.z + p1.z) / 2
            );

            cp = new THREE.Vector3(
                p0.x * 2 - ( startPoint.x + mid.x ) / 2,
                p0.y * 2 - ( startPoint.y + mid.y ) / 2,
                p0.z * 2 - ( startPoint.z + mid.z ) / 2
            );
           
            curvePoints.push( cp );

            curvePoints.push( mid );


            startPoint.x = mid.x; 
            startPoint.y = mid.y;
            startPoint.z = mid.z;

        }
        
		p0 = points[points.length - 2];
        p1 = points[points.length - 1];
      

        cp = new THREE.Vector3(
            p0.x * 2 - ( mid.x + p1.x ) / 2,
            p0.y * 2 - ( mid.y + p1.y ) / 2,
            p0.z * 2 - ( mid.z + p1.z ) / 2
        );

        curvePoints.push( cp );


        var lastPoint = new THREE.Vector3(
            p1.x, 
            p1.y,
            p1.z
        );

        curvePoints.push( lastPoint );

        console.log( curvePoints );

        var geometry = new THREE.BufferGeometry();
        geometry.addAttribute('position',
            new THREE.BufferAttribute(
                new Float32Array(ARC_SEGMENTS * 3),
                3
            )
        );

        var curve = new THREE.CatmullRomCurve3( points );
        curve.curveType = 'catmullrom';
        curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
            color: 0xff000,
            opacity: 0.35,
            linewidth: 2
            } ) ); 
        curve.mesh.castShadow = true;

        scene.add( curve.mesh );

    }

}

