/**
 * 
 * @param  camera :: used to build the main camera  
 * @param  scene  :: used to build the main scene
 * @param  renderer :: display the scene 
 * @param  container :: used to contain a constant variable 
 * @param  light :: used to manage light  
 * @param  stats :: used to provide a simple info box to monitor code performance
 * 
 * */ 
var camera, scene, renderer;
var container, stats;
var light;
var transformControl;

var craftwindowapp = {

    
    /**
     * initiate the renderer 
     * The WebGL renderer displays your beautifully crafted scenes using WebGL.
     *  https://threejs.org/docs/index.html#api/renderers/WebGLRenderer 
     **/ 
    init_renderer: function() {

        container = document.getElementById( 'container' );

            /*
            
            */

        renderer = new THREE.WebGLRenderer( { antialias: true } );
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        renderer.shadowMap.enabled = true;

        container.appendChild( renderer.domElement );

        stats = new Stats();

        container.appendChild( stats.dom );

    }, 

    /**
     * creating scene object to place other staf like camere, lights... 
     * This is where you place objects, lights and cameras.
     * https://threejs.org/docs/index.html#manual/introduction/Creating-a-scene
     * **/ 
    create_scene: function (){
       
        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0xf0f0f0 ); 
    }, 

    create_camera: function(){
        /*This projection mode is designed to mimic the way the human eye sees.
        It is the most common projection mode used for rendering a 3D scene.*/
        // https://threejs.org/docs/index.html#api/cameras/PerspectiveCamera
        camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
        camera.position.set( 0, 250 , 1000 );
        scene.add( camera );
        
    }, 

    create_spotLight: function() {

        // This light globally illuminates all objects in the scene equally.
        // https://threejs.org/docs/index.html#api/lights/AmbientLight
        scene.add( new THREE.AmbientLight( 0xf0f0f0 ) );
    
        //This light gets emitted from a single point in one direction, 
        //along a cone that increases in size the further from the light it gets. 
        // https://threejs.org/docs/index.html#api/lights/SpotLight
        light = new THREE.SpotLight( 0xffffff, 1.5 );
        light.position.set( 0, 1500, 200 );
        light.castShadow = true;
        light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 70, 1, 200, 2000 ) );
        light.shadow.bias = -0.000222;
        light.shadow.mapSize.width = 1024;
        light.shadow.mapSize.height = 1024;
        scene.add( light );
        spotlight = light;
    }, 

    /**
     * A two dimensional surface that extends infinitely in 3d space
     * https://threejs.org/docs/index.html#api/geometries/PlaneBufferGeometry
     */
    create_plane: function (){   
        
        var planeGeometry = new THREE.PlaneBufferGeometry( 2000, 2000 );
        planeGeometry.rotateX( - Math.PI / 2 );
        var planeMaterial = new THREE.ShadowMaterial( { opacity: 0.2 } );
        var plane = new THREE.Mesh( planeGeometry, planeMaterial );
        plane.position.y = -200;
        plane.receiveShadow = true;
        scene.add( plane );

    },


    /**
     * The GridHelper is an object to define grids. Grids are two-dimensional arrays of lines.
     * https://threejs.org/docs/index.html#api/helpers/GridHelper
     */
    create_gridHelper: function() {
        
        var helper = new THREE.GridHelper( 2000, 100 );
        helper.position.y = - 199;
        helper.material.opacity = 0.25;
        helper.material.transparent = true;
        scene.add( helper );
    }, 

    /**
     * An axis object to visualize the 3 axes in a simple way.
     * The X axis is red. The Y axis is green. The Z axis is blue.
     * https://threejs.org/docs/index.html#api/helpers/AxesHelper
     * */
    create_axesHelper: function(){
        
        var axes = new THREE.AxesHelper( 1000 );
        axes.position.set( - 500, - 200, - 500 );
        scene.add( axes );
    
    }, 

    /**
     * Orbit controls allow the camera to orbit around a target.
     */
    control_scene: function() {
        var controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.damping = 0.2;
        controls.addEventListener( 'change', render );
        controls.addEventListener( 'start', function() {
            cancelHideTransorm();
        } );
        controls.addEventListener( 'end', function() {
            delayHideTransform();
        } );
    
    
        transformControl = new THREE.TransformControls( camera, renderer.domElement );
        transformControl.addEventListener( 'change', render );
        scene.add( transformControl );
        
        transformControl.addEventListener( 'change', function( e ) {
        
            cancelHideTransorm();
        
        } );
        
        transformControl.addEventListener( 'mouseDown', function( e ) {
        
            cancelHideTransorm();
        
        } );
        
        transformControl.addEventListener( 'mouseUp', function( e ) {
        
            delayHideTransform();
        
        } );
        
        transformControl.addEventListener( 'objectChange', function( e ) {
        
            updateSplineOutline();
        
        } );
    
        var dragcontrols = new THREE.DragControls( splineHelperObjects, camera, renderer.domElement ); //
        dragcontrols.enabled = false;
        
        dragcontrols.addEventListener( 'hoveron', function ( event ) {
        
            transformControl.attach( event.object );
            cancelHideTransorm();
        
        } );
        
        dragcontrols.addEventListener( 'hoveroff', function ( event ) {
        
            delayHideTransform();
        
        } );
        
        
        var hiding;
        function delayHideTransform() {
            
            cancelHideTransorm();
            hideTransform();
        
        }
        
        function hideTransform() {
        
            hiding = setTimeout( function() {
                
                transformControl.detach( transformControl.object );
            
            }, 2500 )
        }
        
        function cancelHideTransorm() {
           
            if ( hiding ) clearTimeout( hiding );
        
        }
    
    }, 

    render: function () {
           renderer.render( scene, camera );
    }


}